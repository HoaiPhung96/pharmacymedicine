package com.group2.pharmacy.controller;

import com.group2.pharmacy.model.Category;
import com.group2.pharmacy.service.CategoryService;
import com.group2.pharmacy.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    // List Category
    @GetMapping("/admin/category")
    public ModelAndView ListCategory(Pageable pageable) {
        Page<Category> categories;
        categories = categoryService.findAll(pageable);
        ModelAndView modelAndView = new ModelAndView("admin/category/list");
        modelAndView.addObject("listCategory", categories);
        return modelAndView;
    }

    // Create Category
    @GetMapping("/admin/create_category")
    public ModelAndView ShowCreate() {
        ModelAndView modelAndView = new ModelAndView("/admin/category/create");
        modelAndView.addObject("createCategory", new Category());
        return modelAndView;
    }

    @PostMapping("/admin/create_category")
    public ModelAndView CreateCategory(@Validated @ModelAttribute("createCategory") Category category, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/category/create");
            return modelAndView;
        }
        categoryService.save(category);
        ModelAndView modelAndView = new ModelAndView("/admin/category/create");
        modelAndView.addObject("createCategory", new Category());
        modelAndView.addObject("message", "Create Success");
        return modelAndView;
    }

    // Edit Category
    @GetMapping("/admin/edit_category")
    public ModelAndView ShowEdit(@PathVariable Long id) {
        Category category = categoryService.findById(id);
        if (category != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
            modelAndView.addObject("editCategory",category);
            return modelAndView;
        }else {
            ModelAndView modelAndView = new ModelAndView("/admin/error.404");
            return modelAndView;
        }
    }

    @PostMapping("/admin/edit_category")
    public ModelAndView EditCategory(@Validated @ModelAttribute("editCategory")Category category,BindingResult bindingResult){
        if (bindingResult.hasFieldErrors()){
            ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
            return modelAndView;
        }
        categoryService.save(category);
        ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
        modelAndView.addObject("editCategory",category);
        modelAndView.addObject("massage","Edit Success");
        return modelAndView;
    }
    @GetMapping("/admin/delete_category")
    public String deleteCategory(@PathVariable Long id){
        categoryService.remove(id);
        return "redirect:/admin/category";
    }
}
